From http://start.spring.io

Generate : Gradle Project
with Spring Boot: 1.3.1

Search for dependencies: Rabbit -> Stream Rabbit, AMQP

default:
Group: com.myrabbitmq
Artifact: demo

Then drag the unzip demo from Download to Developer/spring_io_project directory

Rename to project name, select build.gradle and drag to Intellij icon to open it quickly

Also rename 

jar {
 baseName = myRabbitMQ
}
